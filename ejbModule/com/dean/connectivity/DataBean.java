package com.dean.connectivity;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.naming.NamingException;

import com.dean.test.CityDAO;
import com.dean.test.CountryDAO;
import com.dean.test.DAOFactory;
import com.dean.test.StorageType;
import com.dean.transferobjects.City;
import com.dean.transferobjects.Country;

/**
 * Session Bean implementation class CityBean
 */
@Stateless
public class DataBean implements DataBeanRemote {

	public static final String JNDI_NAME = DataBean.class.getSimpleName() + "/remote";
	
	@Override
    public List<City> getAllCities() throws EJBException {
		DAOFactory factory = DAOFactory.getDAOFactory(StorageType.MySQL);
    	CityDAO dao = factory.getCityDAO();
    	try {
			return dao.getAllCities();
		} catch (NamingException e) {
			throw new EJBException(e);
		} catch (SQLException e) {
			throw new EJBException(e);
		}
    }

	@Override
	public List<Country> getAllCountries() throws EJBException {
		DAOFactory factory = DAOFactory.getDAOFactory(StorageType.MySQL);
    	CountryDAO dao = factory.getCountryDAO();
    	try {
			return dao.getAllCountries();
		} catch (NamingException e) {
			throw new EJBException(e);
		} catch (SQLException e) {
			throw new EJBException(e);
		}
	}

}
