package com.dean.connectivity;
import java.util.List;

import javax.ejb.EJBException;
import javax.ejb.Remote;

import com.dean.transferobjects.City;
import com.dean.transferobjects.Country;

@Remote
public interface DataBeanRemote {

	public List<City> getAllCities() throws EJBException;
	public List<Country> getAllCountries() throws EJBException;
	
}
